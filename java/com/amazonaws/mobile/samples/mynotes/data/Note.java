/*
 * Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
 * except in compliance with the License. A copy of the License is located at
 *
 *    http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for
 * the specific language governing permissions and limitations under the License.
 */
package com.amazonaws.mobile.samples.mynotes.data;

import android.content.ContentValues;
import android.database.Cursor;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.UUID;

public class Note {
    private long id = -1;
    private String noteId;
    private String title;
    private String content;
    private DateTime created;
    private DateTime updated;

    public static Note fromCursor(Cursor c) {
        Note note = new Note();

        note.setId(getLong(c, NotesContentContract.Notes._ID, -1));
        note.setNoteId(getString(c, NotesContentContract.Notes.NOTEID, ""));
        note.setTitle(getString(c, NotesContentContract.Notes.TITLE, ""));
        note.setContent(getString(c, NotesContentContract.Notes.CONTENT, ""));
        note.setCreated(new DateTime(getLong(c, NotesContentContract.Notes.CREATED, 0), DateTimeZone.UTC));
        note.setUpdated(new DateTime(getLong(c, NotesContentContract.Notes.UPDATED, 0), DateTimeZone.UTC));

        return note;
    }

    private static String getString(Cursor c, String col, String defaultValue) {
        if (c.getColumnIndex(col) >= 0) {
            return c.getString(c.getColumnIndex(col));
        } else {
            return defaultValue;
        }
    }

    private static long getLong(Cursor c, String col, long defaultValue) {
        if (c.getColumnIndex(col) >= 0) {
            return c.getLong(c.getColumnIndex(col));
        } else {
            return defaultValue;
        }
    }

    public Note() {
        setNoteId(UUID.randomUUID().toString());
        setTitle("");
        setContent("");
        setCreated(DateTime.now(DateTimeZone.UTC));
        setUpdated(DateTime.now(DateTimeZone.UTC));
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id;}

    public String getNoteId() { return noteId; }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() { return content; }

    public void setContent(String content) {
        this.content = content;
    }

    public DateTime getCreated() { return created; }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getUpdated() { return updated; }

    public void setUpdated(DateTime updatedDate) {
        this.updated = updatedDate;
    }

    public void updateNote(String title, String content) {
        setTitle(title);
        setContent(content);
        setUpdated(DateTime.now(DateTimeZone.UTC));
    }

    @Override
    public String toString() {
        return String.format("[note#%s] %s", noteId, title);
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put(NotesContentContract.Notes.NOTEID, noteId);
        values.put(NotesContentContract.Notes.TITLE, title);
        values.put(NotesContentContract.Notes.CONTENT, content);
        values.put(NotesContentContract.Notes.CREATED, created.toDateTime(DateTimeZone.UTC).getMillis());
        values.put(NotesContentContract.Notes.UPDATED, updated.toDateTime(DateTimeZone.UTC).getMillis());

        return values;
    }
}
