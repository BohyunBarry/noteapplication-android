/*
 * Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
 * except in compliance with the License. A copy of the License is located at
 *
 *    http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for
 * the specific language governing permissions and limitations under the License.
 */
package com.amazonaws.mobile.samples.mynotes.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;


public class NotesContentContract {

    public static final String AUTHORITY = "com.amazonaws.mobile.samples.mynotes.provider";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final class Notes implements BaseColumns {

        public static final String TABLE_NAME = "notes";

        public static final String _ID = "id";

        public static final String NOTEID = "noteId";

        public static final String TITLE = "title";

        public static final String CONTENT = "content";

        public static final String CREATED = "created";

        public static final String UPDATED = "updated";

        public static final String DIR_BASEPATH = "notes";

        public static final String ITEM_BASEPATH = "notes/*";

        public static final String CREATE_SQLITE_TABLE =
                "CREATE TABLE " + TABLE_NAME + "("
                        + _ID + " INTEGER PRIMARY KEY, "
                        + NOTEID + " TEXT UNIQUE NOT NULL, "
                        + TITLE + " TEXT NOT NULL DEFAULT '', "
                        + CONTENT + " TEXT NOT NULL DEFAULT '', "
                        + CREATED + " BIGINT NOT NULL DEFAULT 0, "
                        + UPDATED + " BIGINT NOT NULL DEFAULT 0)";


        public static final Uri CONTENT_URI =
                Uri.withAppendedPath(NotesContentContract.CONTENT_URI, TABLE_NAME);

        public static final String CONTENT_DIR_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.amazonaws.mobile.samples.notes";

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.amazonaws.mobile.samples.notes";

        public static final String[] PROJECTION_ALL = {
                _ID,
                NOTEID,
                TITLE,
                CONTENT,
                CREATED,
                UPDATED
        };

        public static final String SORT_ORDER_DEFAULT = CREATED + " ASC";

        public static Uri uriBuilder(String noteId) {
            Uri item = new Uri.Builder()
                    .scheme("content")
                    .authority(NotesContentContract.AUTHORITY)
                    .appendPath(NotesContentContract.Notes.DIR_BASEPATH)
                    .appendPath(noteId)
                    .build();
            return item;
        }
    }
}
